const express = require('express')
const router = express.Router()
const Documents = require('../models/Documents')
const DefaultController = require('../controllers/DefaultController')

router.route('/', DefaultController(Documents).createUpdate)

module.exports = router