const express = require('express')
const router = express.Router()
const UserController = require('../controllers/UserController')

router.route('/', UserController.createUpdate)
router.route('/authentication', UserController.authentication)

module.exports = router