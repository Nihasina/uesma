const express = require('express')
const router = express.Router()
const multer = require('multer')
const middleware = require('../middleware')

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads')
    },
    filename: function(req, file, cb) {
        let extArray = file.mimetype.split("/");
        let extension = extArray[extArray.length - 1];
        cb(null, file.fieldname + '-' + Date.now() + '.' + extension)
    }
})
const upload = multer({ storage: storage })

// router.route('/').post(upload.single('file'), (req, res) => {
//     console.log(req.file);
// })
router.route('/').post(middleware, upload.single('file'), (req, res) => {
    return res.json(req.file)
})

module.exports = router