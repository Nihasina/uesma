const express = require('express')
const router = express.Router()
const userRouter = require('./user')
const documentRouter = require('./document')
const uploadRouter = require('./upload')

router.use('/users', userRouter)
router.use('/documents', documentRouter)
router.use('/upload', uploadRouter)

module.exports = router