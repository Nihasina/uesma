const jwt = require('jsonwebtoken')


module.exports = auth = (req, res, next) => {
    const token = req.header('Authorization')

    if (!token) return res.status(401).json({ 'message': 'Anauthenticated user' })

    try {
        const decoded = jwt.verify(token, "x-user-authorization");
        req.user = decoded.user;
        next();
    } catch (e) {
        console.error(e, '<===== Error');
        res.status(200).send({ message: "Invalid Token" });
    }



}