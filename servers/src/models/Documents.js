const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.Promise = global.Promise

const documentSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    filiere: {
        type: String,
        required: true
    },
    file: {
        type: String,
        required: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: Date,
    isRemoved: {
        type: Boolean,
        default: false
    }
})

module.exports = mongoose.model('document', documentSchema)