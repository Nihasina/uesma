const mongoose = require('mongoose')
mongoose.Promise = global.Promise

const userSchema = new mongoose.Schema({
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    sexe: {
        type: Number,
        default: 0
    },
    phone: Number,
    adresse: String,
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: Date,
    isRemoved: {
        type: Boolean,
        default: false
    }

})

module.exports = mongoose.model('user', userSchema)