const bcrypt = require('bcryptjs')
const Users = require('../models/Users')
const jwt = require('jsonwebtoken')

const { error_email, error_mdp } = require('../utils/constrains')
const { errorMessage } = require('../utils/utils')

module.exports = UserController = {

    createUpdate: async(req, res) => {
        const data = req.body

        if (!data._id) {
            const salt = await bcrypt.genSalt(10);
            const password = await bcrypt.hash(req.body.password, salt);
            const newUser = new Users({...data, password: password })
            await newUser.save().then(response => {
                res.json(response)

            }).catch(err => {
                return res.status(201).json(errorMessage(err._message))
            })
        } else {
            Users.findOneAndUpdate({ _id: data._id }, { $set: {...data, updatedAt: Date.now() } }).then(() => {
                return res.json(data)
            }).catch(err => {
                return res.status(201).json(errorMessage(err._message))
            })
        }
    },



    authentication: async(req, res) => {

        const user = await Users.findOne({ email: req.body.email })

        if (user) {
            const isValid = await bcrypt.compare(req.body.password, user.password);
            if (!isValid) {
                return res.status(201).json(errorMessage(error_mdp))
            } else {
                const payload = {
                    user: { _id: user._id }
                }

                jwt.sign(
                    payload,
                    "x-user-authorization", {
                        expiresIn: req.body.remembreMe ? "7d" : "2 days"
                    },
                    (err, token) => {
                        if (err) throw err;
                        res.status(200).json({ token, user })
                    }
                )

            }
        } else {
            return res.status(201).json(errorMessage(error_email))
        }
    },


}