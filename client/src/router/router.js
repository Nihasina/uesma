import { createRouter, createWebHashHistory } from 'vue-router'
import { About, Home } from '../views/pages'
import { Default } from '../views/layouts'

const routes = [{
    path: '/',
    redirect: '/',
    name: 'Default',
    component: Default,
    children: [{
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/about',
            name: 'about',
            component: About
        }
    ]
}]

const router = createRouter({
    history: createWebHashHistory(),
    routes
})

export default router