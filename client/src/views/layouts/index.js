const Default = () =>
    import ('@/views/layouts/Default')
const Navbar = () =>
    import ('@/views/layouts/Navbar')

export {
    Default,
    Navbar
}