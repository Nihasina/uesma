const Home = () =>
    import ('@/views/pages/Home')

const About = () =>
    import ('@/views/pages/About')

export {
    Home,
    About
}